﻿using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using IComponentData = Unity.Entities.IComponentData;

// ReSharper disable once CheckNamespace
namespace EntityProvider.Entities.EntityCreators
{
    [Serializable]
    public class EntityCreator : IEntityCreator
    {
        [SerializeReference] [SerializeReferenceButton]
        private List<IComponentData> _components = new List<IComponentData>();

        public Entity CreateEntity(EntityManager entityManager, string name)
        {
            var entity = entityManager.CreateEntity();
#if UNITY_EDITOR
            entityManager.SetName(entity, name);
#endif

            var conversionSystem =
                World.DefaultGameObjectInjectionWorld.GetExistingSystem<GameObjectConversionSystem>();

            foreach (var component in _components)
            {
                if (component is IConvertGameObjectToEntity convertGameObjectToEntity)
                {
                    convertGameObjectToEntity.Convert(entity, entityManager, conversionSystem);
                    continue;
                }

                if (component.GetType().IsClass)
                {
                    entityManager.AddComponentObject(entity, component);
                    continue;
                }

                Debug.LogError(
                    $"[{nameof(EntityCreator)}] Component '{component}' must be implement '{nameof(IConvertGameObjectToEntity)}'" +
                    " or nullable type object");
            }

            return entity;
        }
    }
}