﻿using Unity.Entities;

// ReSharper disable once CheckNamespace
namespace EntityProvider.Entities.EntityCreators
{
    public interface IEntityCreator
    {
        Entity CreateEntity(EntityManager entityManager, string name);
    }
}