using System;
using Command;
using ComponentWrapper;
using EntityProvider.Entities.EntityCreators;
using Unity.Entities;
using UnityEngine;

// Overrides the default conversion logic. Allows manual addition of a component to the entity.
// ReSharper disable once CheckNamespace
namespace EntityProvider.Entities
{
    [Serializable]
    public class EntityProvider : IEntityCreator, IComponentObjectWrapper, IComponentDataWrapper<IComponentData>,
        ICommandHandler<IComponentData>
    {
        [SerializeReference] private IEntityCreator _entityCreator = new EntityCreator();

        private Entity _entity;
        private EntityManager _entityManager;

        public Entity CreateEntity(EntityManager entityManager, string name)
        {
            _entityManager = entityManager;
            _entity = _entityCreator.CreateEntity(_entityManager, name);

            return _entity;
        }

        public TComponent GetComponentObject<TComponent>() where TComponent : class
        {
            return _entityManager.GetComponentObject<TComponent>(_entity);
        }

        public TComponent GetComponentData<TComponent>() where TComponent : struct, IComponentData
        {
            return _entityManager.GetComponentData<TComponent>(_entity);
        }

        public void OnCommand<TCommand>(TCommand command) where TCommand : struct, IComponentData
        {
            _entityManager.AddComponentData(_entity, command);
        }
    }
}