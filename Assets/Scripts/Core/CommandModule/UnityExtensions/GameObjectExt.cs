﻿using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Command.UnityExtensions
{
    public static class GameObjectExt
    {
        public static void OnCommand<TCommand, TConstraint>(this GameObject gameObj, TCommand command)
            where TCommand : struct, TConstraint
        {
            var commandHandlers = gameObj.GetComponents<ICommandHandler<TConstraint>>();
            foreach (var commandHandler in commandHandlers)
            {
                commandHandler.OnCommand(command);
            }
        }
    }
}