﻿// ReSharper disable once CheckNamespace

namespace Command
{
    public interface ICommandHandler<in TConstraint>
    {
        void OnCommand<TCommand>(TCommand command) where TCommand : struct, TConstraint;
    }

    public interface ICommandHandler : ICommandHandler<ICommand>
    {
    }
}