﻿// ReSharper disable once CheckNamespace

namespace Valuable
{
    public interface IValueChangeable<T>
    {
        float Diff { get; }
    }
}