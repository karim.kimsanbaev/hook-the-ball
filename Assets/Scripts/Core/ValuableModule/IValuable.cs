﻿// ReSharper disable once CheckNamespace

namespace Valuable
{
    public interface IValuable
    {
        float Value { get; }
    }
}