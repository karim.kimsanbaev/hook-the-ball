﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace ComponentWrapper.Entities.UnityExtensions
{
    public static class GameObjectExt
    {
        public static TComponent GetComponentObject<TComponent>(this GameObject gameObj)
            where TComponent : class
        {
            var success = gameObj.TryGetComponent<IComponentObjectWrapper>(out var componentWrapper);
            if (!success)
            {
                throw new ArgumentException(
                    $"Not found '{nameof(IComponentObjectWrapper)}' component in Game Object '{gameObj.name}'");
            }

            return componentWrapper.GetComponentObject<TComponent>();
        }

        public static TComponent GetComponentData<TComponent, TConstraint>(this GameObject gameObj)
            where TComponent : struct, TConstraint
        {
            var success = gameObj.TryGetComponent<IComponentDataWrapper<TConstraint>>(out var componentWrapper);
            if (!success)
            {
                throw new ArgumentException(
                    $"Not found '{nameof(IComponentDataWrapper<TConstraint>)}' component in Game Object '{gameObj.name}'");
            }

            return componentWrapper.GetComponentData<TComponent>();
        }
    }
}