﻿// ReSharper disable once CheckNamespace

namespace ComponentWrapper
{
    public interface IComponentObjectWrapper
    {
        TComponent GetComponentObject<TComponent>() where TComponent : class;
    }
}