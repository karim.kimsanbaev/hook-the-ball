﻿// ReSharper disable once CheckNamespace

namespace ComponentWrapper
{
    public interface IComponentDataWrapper<in TConstraint>
    {
        TComponent GetComponentData<TComponent>() where TComponent : struct, TConstraint;
    }
}