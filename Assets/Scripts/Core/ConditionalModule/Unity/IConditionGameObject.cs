﻿using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Conditional.Unity
{
    public interface IConditionGameObject : ICondition<GameObject>
    {
    }
}