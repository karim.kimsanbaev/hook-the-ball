﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Conditional.Unity
{
    [Serializable]
    public class GameObjectTagCondition : IConditionGameObject
    {
        [SerializeField] private string tag;

        public bool IsTrue(GameObject param)
        {
            return param.CompareTag(tag);
        }
    }
}