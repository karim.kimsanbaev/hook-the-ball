﻿// ReSharper disable once CheckNamespace

namespace Conditional
{
    public interface ICondition<in TParam>
    {
        bool IsTrue(TParam param);
    }
}