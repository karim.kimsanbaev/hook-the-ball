﻿using System;
using Unity.Entities;
using Unity.Jobs;

namespace EntitiesExtension.CommandBuffer
{
    public static class CommandBuffer
    {
        public static EntityCommandBuffer GetBuffer(this World world, Type bufferType = default)
        {
            bufferType ??= typeof(EndFixedStepSimulationEntityCommandBufferSystem);

            var buffer = world.GetOrCreateSystem(bufferType) as EntityCommandBufferSystem;
            return buffer!.CreateCommandBuffer();
        }

        public static void AddDependency(this World world, JobHandle dependency, Type bufferType = default)
        {
            bufferType ??= typeof(EndFixedStepSimulationEntityCommandBufferSystem);

            var buffer = world.GetOrCreateSystem(bufferType) as EntityCommandBufferSystem;
            buffer!.AddJobHandleForProducer(dependency);
        }
    }
}