using Command;
using ComponentWrapper;
using Unity.Entities;
using UnityEngine;

// Overrides the default conversion logic. Allows manual addition of a component to the entity.
// ReSharper disable once CheckNamespace
namespace Avatar.Entities
{
    public class Avatar : ConvertToEntity, IComponentObjectWrapper, IComponentDataWrapper<IComponentData>,
        ICommandHandler<IComponentData>
    {
        [SerializeReference] private EntityProvider.Entities.EntityProvider entityProvider =
            new EntityProvider.Entities.EntityProvider();

        private EntityManager _entityManager;
        private Entity _entity;

        private void Awake()
        {
            _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            _entity = entityProvider.CreateEntity(_entityManager, name);

            // Adding avatar to link avatar with entity.
            _entityManager.AddComponentObject(_entity, this);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            ConversionMode = Mode.ConvertAndInjectGameObject;
        }
#endif

        private void Update()
        {
            // Destroy GameObject if entity is not exists.
            if (_entityManager.Exists(_entity))
            {
                return;
            }

            Destroy(gameObject);
        }

        public TComponent GetComponentObject<TComponent>() where TComponent : class
        {
            return entityProvider.GetComponentObject<TComponent>();
        }

        public TComponent GetComponentData<TComponent>() where TComponent : struct, IComponentData
        {
            return entityProvider.GetComponentData<TComponent>();
        }

        public void OnCommand<TCommand>(TCommand command) where TCommand : struct, IComponentData
        {
            entityProvider.OnCommand(command);
        }
    }
}