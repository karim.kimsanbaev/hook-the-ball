﻿using UnityEngine;

// ReSharper disable once CheckNamespace
namespace PlayerInput
{
    public class PlayerInputLeftRightMovement : MonoBehaviour
    {
        [SerializeField] private float speed;

        private void Update()
        {
            var horizontal = Input.GetAxis("Horizontal");
            var position = transform.position;
            transform.position = new Vector3(position.x + horizontal * speed * Time.deltaTime, position.y, position.z);
        }
    }
}