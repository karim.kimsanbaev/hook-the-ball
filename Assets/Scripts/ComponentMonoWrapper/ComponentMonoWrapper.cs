using ComponentWrapper;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace ComponentMonoWrapper
{
    public class ComponentMonoWrapper : MonoBehaviour, IComponentObjectWrapper
    {
        private new TComponent GetComponent<TComponent>()
        {
            var success = TryGetComponent<TComponent>(out var component);
            if (success)
            {
                return component;
            }

            return default;
        }

        public TComponent GetComponentObject<TComponent>() where TComponent : class => GetComponent<TComponent>();
    }
}