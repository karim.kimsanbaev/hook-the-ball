using System;
using Unity.Entities;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable once CheckNamespace
namespace SpawnModule
{
    public class SpawnerInSquareBehaviour : MonoBehaviour, IComponentData
    {
        [SerializeField] private float spawnPerSecond;

        [SerializeField] public GameObject prefab;

        private TimeSpan _lastSpawnedTs;

        private BoxCollider2D _boxCollider2D;

        private void Start()
        {
            TryGetComponent(out _boxCollider2D);
        }

        private void Update()
        {
            var utcNow = DateTime.UtcNow.TimeOfDay;

            var subtract = utcNow.Subtract(_lastSpawnedTs);
            if (!(subtract.Seconds >= spawnPerSecond))
            {
                return;
            }

            _lastSpawnedTs = utcNow;

            var bounds = _boxCollider2D.bounds;
            var min = bounds.min;
            var max = bounds.max;

            Instantiate(prefab,
                new Vector3(
                    Random.Range(min.x, max.x),
                    Random.Range(min.y, max.y),
                    Random.Range(min.z, max.z)
                ), Quaternion.identity);
        }
    }
}