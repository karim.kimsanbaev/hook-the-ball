using System;
using Command;
using UniRx;
using Unity.Entities;
using UnityEngine;
using Valuable;

// ReSharper disable once CheckNamespace
namespace HealthModule
{
    public struct Health : IComponentData, IValuable, IObservable<float>,
        ICommandHandler<IValueChangeable<Health>>, IConvertGameObjectToEntity
    {
        [field: SerializeField] public float Value { get; private set; }

        private Entity _entity;

        public void OnCommand<TCommand>(TCommand command) where TCommand : struct, IValueChangeable<Health>
        {
            Value += command.Diff;
        }

        public IDisposable Subscribe(IObserver<float> observer)
        {
            var changeHealthSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<ObservableHealthSystem>();

            var observable = changeHealthSystem.GetObservableForEntity(_entity);
            return observable.StartWith(Value).Subscribe(observer);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            _entity = entity;
            dstManager.AddComponentData(entity, this);
        }
    }
}