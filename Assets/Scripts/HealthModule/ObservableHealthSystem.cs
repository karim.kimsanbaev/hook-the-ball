﻿using System;
using System.Collections.Concurrent;
using UniRx;
using Unity.Entities;

// ReSharper disable once CheckNamespace
namespace HealthModule
{
    public partial class ObservableHealthSystem : SystemBase
    {
        private readonly ConcurrentDictionary<Entity, Subject<float>> _observers =
            new ConcurrentDictionary<Entity, Subject<float>>();

        private EntityQuery _query;

        protected override void OnCreate()
        {
            _query = GetEntityQuery(ComponentType.ReadOnly<Health>());
            _query.SetChangedVersionFilter(typeof(Health));
        }

        protected override void OnUpdate()
        {
            Entities.WithStoreEntityQueryInField(ref _query).ForEach(
                (in Health health, in Entity entity) =>
                {
                    var success = _observers.TryGetValue(entity, out var subject);
                    if (success)
                    {
                        subject.OnNext(health.Value);
                    }
                }).WithoutBurst().Run();
        }

        internal IObservable<float> GetObservableForEntity(Entity entity)
        {
            return _observers.GetOrAdd(entity, _ => new Subject<float>());
        }
    }
}