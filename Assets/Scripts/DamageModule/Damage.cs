﻿using System;
using Command;
using HealthModule;
using Unity.Entities;
using UnityEngine;
using Valuable;

// ReSharper disable once CheckNamespace
namespace DamageModule
{
    [Serializable]
    public struct Damage : IComponentData, ICommand, IValuable, IValueChangeable<Health>, IConvertGameObjectToEntity
    {
        public Damage(float value)
        {
            Value = value;
        }

        [field: SerializeField] public float Value { get; set; }
        public float Diff => Value * -1;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, this);
        }
    }
}