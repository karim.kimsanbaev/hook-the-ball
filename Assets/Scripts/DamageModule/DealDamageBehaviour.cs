using Command.UnityExtensions;
using Unity.Entities;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace DamageModule
{
    public class DealDamageBehaviour : MonoBehaviour
    {
        [SerializeField] private float damage;

        public void DealDamageTo(GameObject gameObj)
        {
            var damageCommand = new Damage(damage);
            gameObj.OnCommand<Damage, IComponentData>(damageCommand);
        }
    }
}