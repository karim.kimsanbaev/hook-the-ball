﻿using EntitiesExtension.CommandBuffer;
using HealthModule;
using Unity.Entities;

// ReSharper disable once CheckNamespace
namespace DamageModule
{
    public partial class DamageSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Health health, ref Damage damage) =>
            {
                health.OnCommand(damage);
                damage.Value = 0;
            }).ScheduleParallel();

            var buffer = World.GetBuffer();
            Entities.ForEach((in Damage damage, in Entity entity) =>
            {
                if (damage.Value == 0)
                {
                    buffer.RemoveComponent<Damage>(entity);
                }
            }).Schedule();
        
            World.AddDependency(Dependency);
        }
    }
}