﻿using Unity.Entities;
using UnityEngine;
using Valuable;

// ReSharper disable once CheckNamespace
namespace Pickup.Components
{
    public struct Coin : IComponentData, IPickable, IValuable, IValueChangeable<Wallet>, IConvertGameObjectToEntity
    {
        private Entity _entity;
        private EntityManager _entityManager;
        
        public void OnPickup()
        {
            _entityManager.DestroyEntity(_entity);
        }

        [field: SerializeField] public float Value { get; private set; }

        public float Diff => Value;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            _entity = entity;
            _entityManager = dstManager;
            
            dstManager.AddComponentData(entity, this);
        }
    }
}