using System;
using Command;
using Unity.Entities;
using UnityEngine;
using Valuable;

// ReSharper disable once CheckNamespace
namespace Pickup.Components
{
    public struct Wallet : IComponentData, ICommandHandler, IValuable, IConvertGameObjectToEntity
    {
        [field: SerializeField] public float Value { get; private set; }

        public void OnCommand<TCommand>(TCommand command) where TCommand : struct, ICommand
        {
            if (!(command is Commands.PickupCoin pickupCommand))
            {
                throw new InvalidCastException(
                    $"[{nameof(Wallet)}] '{nameof(command)}' can't cast of type '{nameof(Pickup)}'");
            }

            Value += pickupCommand.Value;
            pickupCommand.OnPickup();
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, this);
        }
    }
}