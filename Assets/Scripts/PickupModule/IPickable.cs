﻿// ReSharper disable once CheckNamespace
namespace Pickup
{
    public interface IPickable
    {
        void OnPickup();
    }
}