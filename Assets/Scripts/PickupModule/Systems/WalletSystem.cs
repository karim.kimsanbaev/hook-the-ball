﻿using Pickup.Components;
using Unity.Entities;

// ReSharper disable once CheckNamespace
namespace Pickup.Systems
{
    public partial class WalletSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.WithStructuralChanges().ForEach((ref Wallet wallet, in Commands.PickupCoin pickup, in Entity entity) =>
            {
                wallet.OnCommand(pickup);
                //EntityManager.SetComponentData<Wallet>(entity, wallet);
                EntityManager.RemoveComponent<Commands.PickupCoin>(entity);
            }).WithoutBurst().Run();
        }
    }
}