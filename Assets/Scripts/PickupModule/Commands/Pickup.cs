﻿using Command;
using Pickup.Components;
using Unity.Entities;
using Valuable;

// ReSharper disable once CheckNamespace
namespace Pickup.Commands
{
    public struct PickupCoin : IComponentData, ICommand, IPickable, IValuable
    {
        public float Value => _coin.Value;

        private Coin _coin;

        public PickupCoin(Coin coin)
        {
            _coin = coin;
        }

        public void OnPickup()
        {
            _coin.OnPickup();
        }
    }
}