﻿using Command;
using ComponentWrapper.Entities.UnityExtensions;
using Pickup.Components;
using Unity.Entities;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Pickup.Behaviours
{
    public class PickupBehaviour : MonoBehaviour
    {
        [SerializeField] private Object avatar;

        public void Pickup(GameObject gameObj)
        {
            Debug.Log($"[{nameof(PickupBehaviour)}] Pickup '{gameObj.name}'");

            var coin = gameObj.GetComponentData<Coin, IComponentData>();

            var pickup = new Commands.PickupCoin(coin);
            ((ICommandHandler<IComponentData>) avatar).OnCommand(pickup);
        }
    }
}