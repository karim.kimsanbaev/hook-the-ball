using System;
using UnityEngine;
using UnityEngine.Events;

// ReSharper disable once CheckNamespace
namespace UnityEvents
{
    [Serializable]
    public class UnityGameObjectEvent : UnityEvent<GameObject>
    {
    }
}