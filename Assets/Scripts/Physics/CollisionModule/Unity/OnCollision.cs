using UnityEngine;
using UnityEvents;

// ReSharper disable once CheckNamespace
namespace Collision.Unity
{
    public class OnCollision : MonoBehaviour
    {
        [SerializeField] private UnityGameObjectEvent onCollisionEnter2D;

        private void OnCollisionEnter2D(Collision2D collision) => onCollisionEnter2D.Invoke(collision.gameObject);
    }
}