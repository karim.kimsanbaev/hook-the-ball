using Conditional.Unity;
using JetBrains.Annotations;
using UnityEngine;
using UnityEvents;

// ReSharper disable once CheckNamespace
namespace Collision.Unity.Conditional
{
    public class OnCollisionConditional : MonoBehaviour
    {
        [SerializeReference] [SerializeReferenceButton] [UsedImplicitly]
        private IConditionGameObject _condition;

        [SerializeField] private UnityGameObjectEvent onCollisionEnter;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (_condition.IsTrue(collision.gameObject))
            {
                onCollisionEnter.Invoke(collision.gameObject);
            }
        }
    }
}