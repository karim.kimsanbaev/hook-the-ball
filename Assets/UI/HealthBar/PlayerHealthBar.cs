using System;
using ComponentWrapper.Entities.UnityExtensions;
using HealthModule;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace UI.HealthBar
{
    [RequireComponent(typeof(UIDocument))]
    public class PlayerHealthBar : UIBehaviour, IObserver<float>
    {
        private IDisposable _unsubscriber;
        private BaseField<float> _valueWidget;

        protected override void Start()
        {
            var playerGo = GameObject.FindWithTag("Player");
            var health = playerGo.GetComponentData<Health, IComponentData>();

            _unsubscriber = health.Subscribe(this);
        }

        protected override void OnEnable()
        {
            var success = TryGetComponent<UIDocument>(out var uiDocument);
            if (!success)
            {
                return;
            }

            _valueWidget = uiDocument.rootVisualElement.Q<BaseField<float>>();
        }

        protected override void OnDisable() => _unsubscriber.Dispose();

        public void OnCompleted()
        {
        }

        public void OnError(Exception error)
        {
        }

        public void OnNext(float healthValue)
        {
            _valueWidget.value = healthValue;
        }
    }
}